const restore = require('mongodb-restore');
const backup = require('mongodb-backup');


backup({
  uri: '', // mongodb://<dbuser>:<dbpassword>@<dbdomain>.mongolab.com:<dbport>/<dbdatabase>
  root: './',
  callback: (err)=>{
      if(err){
          console.log(err)
      }
      else{
      restore({
        uri: '', // mongodb://<dbuser>:<dbpassword>@<dbdomain>.mongolab.com:<dbport>/<dbdatabase>
        root: './pgdemo',
        callback: (err)=>{
        if(err){
            console.log(err)
        }
        else{
            console.log("Done")
        }
        }
      });

    }
  }
})

// restore({
//   uri: 'mongodb://localhost/pgdemo', // mongodb://<dbuser>:<dbpassword>@<dbdomain>.mongolab.com:<dbport>/<dbdatabase>
//   root: './pgdemo'
// });
